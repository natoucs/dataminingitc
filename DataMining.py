"""Main file for Data Mining Project"""

import requests
from bs4 import BeautifulSoup
import ast
import pandas as pd
import click
import datetime
import os
import logging
import json
import SQLPopulation

SORT_OPTIONS = ['most_funded', 'newest', 'most_backed', 'end_date', 'popularity', 'magic']

SITE_CATEGORIES = {'Art': 1, 'Comics': 3, 'Dance': 6, 'Design': 7,
                   'Fashion': 9, 'Food': 10, 'Games': 12, 'Journalism': 13,
                   'Music': 14, 'Photography': 15, 'Technology': 16,
                   'Theater': 17, 'Publishing': 18, 'Crafts': 26}


@click.command()
@click.option('-c', '--category', type=click.Choice(list(SITE_CATEGORIES.keys())), default='Technology',
              help="Get only a specific category of projects, if not given all will be searched.")
@click.option('-s', '--sort', type=click.Choice(SORT_OPTIONS), default=SORT_OPTIONS[0],
              help="Select which type of projects to prioritize in returning")
@click.option('-p', '--pages', default=1,
              help="Select number of pages to scrap from")
@click.option('-u', '--database_username', default='ITC',
              help="Username for connecting to the database")
@click.option('-w', '--write_to_file', type=click.Choice([True, False]), default=False,
              help="Whether to write the results to a file")
def set_up(sort, category, pages, database_username, write_to_file):
    """Creates website URL for scraping based on parameters
    Creates filename to store web results based on parameters"""
    # Set up logging
    logging.basicConfig(level=logging.INFO,
                        format='%(message)s',
                        filename='datamining_report.log',
                        filemode='w')
    logging.info('Scraping %i page from category %s with sort by %s using database_username %s and writing to file set to %s'
                 % (pages, category, sort, database_username, write_to_file))
    now_datetime = datetime.datetime.now()
    # Loop through to get all the websites to scrape
    if category is None:
        categories_to_scrape = list(SITE_CATEGORIES.values())
    else:
        categories_to_scrape = list([SITE_CATEGORIES[category]])
    my_scraper = KickstarterScraper()
    websites_to_scrape = my_scraper.build_website(categories_to_scrape, sort, pages)
    # Create to store data frames returned from each site to combine later
    df_to_combine = list()
    for website_url in websites_to_scrape:
        logging.info("Currently scraping %s" % (website_url, ))
        df = my_scraper.scrape_website(website_url)
        if df is not None:
            df_to_combine.append(df)
    # Check how many data frames of data were successfully captured
    if len(df_to_combine) == 0:
        return
    elif len(df_to_combine) == 1:
        df = df_to_combine[0]
    else:
        df = pd.concat(df_to_combine, ignore_index=True)
    df = df.drop_duplicates()
    # Gather the additional Creator Information
    creator_df = my_scraper.creator_activity(set(df.creatorurlswebuser))
    # Full Info contains all the projects with their creator info
    full_info = pd.merge(df, creator_df, how='left', left_on='creatorurlswebuser', right_on='creator_url')
    f = '%Y-%m-%d %H:%M:%S'
    full_info['scrape'] = now_datetime.strftime(f)
    if write_to_file:
        create_file(category, full_info, now_datetime, sort)
    full_info = full_info.where((pd.notnull(full_info)), None)
    logging.info('Before adding to DB, we had %i rows of content' % full_info.shape[0])
    countries_population = my_scraper.get_countries_population(set(full_info.country))
    # returns df with col=[code, name, population] of country
    my_database = SQLPopulation.SQLDatabaseManagement(database_username, full_info, countries_population)
    logging.info("Database Ready for population: %s" % (str(my_database.database_ready), ))
    success = my_database.sql_population()
    logging.info("Successful Database Population: " + str(success))
    return


def create_file(category, full_info, now_datetime, sort):
    """Creates a file with the webscraping results"""
    category_label = 'All' if category is None else category
    file_name = category_label + '_' + sort + now_datetime.strftime('_%d%m%Y_%H%M') + r'.csv'
    with open(file_name, 'a', encoding="utf-8") as f:
        full_info.to_csv(f, index=False, header=True)
    f.close()
    print("File Results: ", os.path.abspath(file_name))
    logging.info("File Results: " + str(os.path.abspath(file_name)))


class KickstarterScraper:
    def __init__(self):
        """Setup the variables for later"""
        self.website_base = 'https://www.kickstarter.com/discover/advanced?state=live'
        self.info_needed = ['id', 'name', 'blurb', 'goal', 'pledged', 'backers_count', 'percent_funded', 'usd_pledged',
                            'currency', 'current_currency', 'currency_symbol',
                            'is_starrable', 'staff_pick', 'spotlight', 'slug',
                            'created_at', 'launched_at', 'deadline', 'state', 'country',
                            ['location', ['name', 'country', 'state', 'displayable_name']],
                            ['category', ['name', 'slug', 'id', 'parent_id']],
                            ['creator', ['name', 'id', ['urls', [['web', ['user']]]]]],
                            ['urls', [['web', ['project']]]]]
        self.creator_labels = ['Backed', 'Comments', 'Created']
        self.country_code = 'http://country.io/names.json'
        self.api_population = 'http://api.population.io:80/1.0/population/'

    def build_website(self, category_values, sort, pages):
        """ Returns a list of websites to scrape"""
        website_urls = list()
        for category in category_values:
            for page in range(1, pages + 1):
                website_url = self.website_base + r'&sort=' + sort
                website_url += r'&category_id=' + str(category) + r'&page=' + str(page)
                website_urls.append(website_url)
        return website_urls

    def scrape_website(self, website_url):
        """Takes a URL and file_name, scrapes the website and
        returns a Pandas dataframe of the data"""
        soup = self.extract_content_from_url(website_url)  # extract projects raw information
        if soup is None:
            return
        all_projects = soup.find_all(class_="js-react-proj-card")  # ResultSet object
        if all_projects is None or len([all_projects]) == 0:
            logging.info("No projects found.")
            return
        df = self.fill_df(all_projects)  # fill the database with projects content
        return df

    @staticmethod
    def extract_content_from_url(url):
        """Extract projects raw information"""
        try:
            r = requests.get(url)
            soup = BeautifulSoup(r.content, 'html.parser')  # BeautifulSoup object
            return soup
        except requests.exceptions.HTTPError:
            logging.info("Invalid URL: " + str(url))
            return None
        except requests.exceptions.ConnectionError as errc:
            logging.info("Error Connecting: " + str(errc.strerror))
            return None
        except requests.exceptions.Timeout as errt:
            logging.info("Timeout Error: " + str(errt.strerror))
            return None
        except requests.exceptions.RequestException as err:
            logging.info("Error Retrieving URL data " + str(err.strerror))
            return None

    def fill_df(self, all_projects):
        """Returns a Pandas Dataframe with the extracted projects content"""
        db_values = dict()
        for i in range(len(all_projects)):  # extract info project by project
            one_project = all_projects[i]
            attributes = one_project.attrs['data-project']  # returns an unicode string
            formatted_attributes = self.formatter(attributes)
            project_dict = ast.literal_eval(formatted_attributes)  # transforms the string into a dictionary
            df = self.attribute_dict(project_dict, self.info_needed)  # use the dictionary to fill the database
            # format the timestamps here? datetime.date.fromtimestamp(t)
            db_values[i] = df
        df = pd.DataFrame(db_values).T
        return df

    @staticmethod
    def formatter(s):
        """Sanitises an unicode string extracted from the website"""
        s = s.replace('false', 'False').replace('true', 'True')
        s = s.replace('null', 'None')
        return s

    def attribute_dict(self, dict_to_parse, to_include, attribute_values=None, key_join=''):
        """Finds the values for the keys supplied in the to_include
        key_join, word to add keys supplied by to_include when storing"""
        if attribute_values is None:
            attribute_values = dict()
        for item_key in to_include:
            if isinstance(item_key, list):
                # the key corresponds to a dictionary value
                # Will get relevant values from nested dictionary
                second_dict = dict_to_parse.get(item_key[0], None)
                if second_dict is None:
                    second_dict = dict()
                attribute_values = self.attribute_dict(second_dict, item_key[1],
                                                       attribute_values, key_join + item_key[0])
            else:
                attribute_values[key_join + item_key] = dict_to_parse.get(item_key, None)
        return attribute_values

    def creator_activity(self, urls):
        """Get the creator backed and started"""
        logging.info("Starting to scrape Creator websites.")
        dictionary_series = dict()
        for url_index, url in enumerate(urls):
            logging.info("Currently scraping %s" % (url, ))
            soup = self.extract_content_from_url(url)
            if soup is None:
                continue
            content = soup.find_all('li', class_="nav--subnav__item")  # ResultSet object
            if len(content) < 1:
                continue
            found_values = {'creator_url': url}
            found_values = self.creator_content(content, found_values, self.creator_labels)
            dictionary_series[url_index] = found_values
        creator_df = pd.DataFrame(dictionary_series).T
        return creator_df

    @staticmethod
    def creator_content(content, found_values, labels):
        """Loop through the creator content and add to dictionary
        return the dictionary of values"""
        for li in content:
            links = li.find_all('a')
            for link in links:
                link_info = link.text.split()
                if len(link_info) != 2:
                    continue
                label = link_info[0]
                if label in labels:
                    found_values[link_info[0]] = link_info[1]
        return found_values

    def get_countries_population(self, df):
        """API usecase: gets list of country codes and returns their population
        in a dataframe with columns [code, name, population]"""

        code_to_name_countries = json.loads(requests.get(self.country_code).content.decode('utf-8'))
        code_to_name_countries['NL'] = 'The Netherlands'  # prevent error because some names differ
        code_to_name_countries['HK'] = 'Hong Kong SAR-China'
        today_date = datetime.datetime.today().strftime('%Y-%m-%d')
        big_df = pd.DataFrame(columns=['code', 'name', 'population'])
        for code in df:  # translate every country to its name and use name to get population from the API
            name = code_to_name_countries[code]
            url = self.api_population + name + '/' + today_date
            pop_info = json.loads(requests.get(url).content.decode('utf-8'))
            try:
                population = pop_info['total_population']['population']
                big_df = big_df.append({'code': code, 'name': name, 'population': population}, ignore_index=True)
            except KeyError:
                print('could not append following country code to dataframe:', code)
                logging.info('could not find country name for this country code:' + str(code))
        return big_df


def main():
    # Don't want to run set_up when importing to execute tests
    pass


if __name__ == "__main__":
    """Only run if not imported."""
    main()
    set_up()
