"""File for Data Mining Project
Scrapes the Kickstarter website"""


import requests
from bs4 import BeautifulSoup
import re
import pandas as pd


URL = "https://www.kickstarter.com/discover/categories/technology?ref=discovery_overlay"
ATTRIBUTES_TO_FETCH = ['"id"', 'name"', 'blurb"', 'goal"', 'pledged"', 'state"', 'slug"',
                       'country"', 'currency"', 'currency_symbol"', 'deadline"', 'created_at"',
                       'launched_at"', 'staff_pick"', 'is_starrable"', 'backers_count"',
                       'usd_pledged"', 'converted_pledged_amount"', 'current_currency"', 'spotlight"', 'percent_funded"']
                       #['creator"', ['{"id"', ''"name"'']], ['location"', ['{"id"', '"name"']],
                       #['category"', ['{"id"', '"name"', '"slug"']],


def webpage_content(url):
    """Scans the webpage and builds a panda dataframe that is returned"""
    webpage = requests.get(url)
    url_html = BeautifulSoup(webpage.content, 'html.parser')
    kickstarter_cards = url_html.find_all('div', class_='js-react-proj-card')
    web_data = list()
    for card in kickstarter_cards:
        attributes = card.attrs['data-project']
        card_data = parse_attributes(attributes[1:-1])
        card_series = create_dataframe(card_data, ATTRIBUTES_TO_FETCH)
        web_data.append(card_series)
    web_columns = [a.replace('"', '') for a in ATTRIBUTES_TO_FETCH]
    web_output = pd.DataFrame(web_data, columns=web_columns, index=list(range(len(web_data))))
    return web_output


def parse_attributes(attributes):
    """Parse out the string attributes"""
    pattern3 = r'({[^{]+{[^{]+{[^{]+}}})'
    pattern2 = r'({[^{]+{[^{]+}})'
    pattern1 = r'({.+?})'
    attributes = sub_strings(attributes, pattern3)
    attributes = sub_strings(attributes, pattern2)
    attributes = sub_strings(attributes, pattern1)
    attributes = attributes.split(',"')
    separated_attributes = {a[:a.find(":")]: a[a.find(":") + 1:] for a in attributes}
    for key, value in separated_attributes.items():
        if value.find("|") != -1:
            value = {x[:x.find(":")]: x[x.find(":") + 1:] for x in value.split("|")}
            separated_attributes[key] = value
    return separated_attributes


def sub_strings(string_to_sub, pattern, to_replace=",", with_replace="|"):
    patterns_found = re.findall(pattern, string_to_sub)
    if patterns_found is None:
        return string_to_sub
    for pattern in patterns_found:
        sub_string = pattern.replace(to_replace, with_replace)
        string_to_sub = string_to_sub.replace(pattern, sub_string)
    return string_to_sub


def create_dataframe(dictionary_to_search, keys_to_include):
    series_for_dataframe = list()
    for key in keys_to_include:
        series_for_dataframe.append(dictionary_to_search.get(key, None))
    return series_for_dataframe


def main():
    web_data = webpage_content(URL)
    print(web_data)


if __name__ == "__main__":
    main()
