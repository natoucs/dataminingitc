"""File for SQL Population in the Data Mining Project"""

import mysql.connector
from mysql.connector import errorcode


class SQLDatabaseManagement:
    def __init__(self, database_username, database_content, country_content):
        """Sets the appropriate variables and ensures the database is set-up"""
        self.database_username = database_username
        self.database_content = database_content
        self.country_content = country_content
        self.database_creation_query = ["SET default_storage_engine = MyISAM;",
                                        "CREATE DATABASE IF NOT EXISTS kickstarter;",
                                        "USE kickstarter;",
                                        """CREATE TABLE IF NOT EXISTS category_mappings (
                            ID INT  AUTO_INCREMENT,
                            category_id INT NOT NULL,
                            category_name VARCHAR(255) NOT NULL,
                            category_parent_id INT,
                            category_slug VARCHAR(255) NOT NULL,
                            PRIMARY KEY (ID),
                            UNIQUE KEY(category_id)
                            );""",
                                        """CREATE TABLE IF NOT EXISTS creator (
                            ID INT  AUTO_INCREMENT,
                            creator_id BIGINT,
                            creator_name VARCHAR(255),
                            creator_url VARCHAR(255),
                            backed_projects INT,
                            created_projects INT,
                            comments INT,
                            PRIMARY KEY (ID),
                            UNIQUE KEY(creator_id)
                            );""",
                                        """CREATE TABLE IF NOT EXISTS projects (
                         ID INT  AUTO_INCREMENT,
                         project_ID BIGINT NOT NULL,
                         name VARCHAR(255),
                         slug VARCHAR(255),
                         blurb TEXT,
                         category_id INT,
                         country VARCHAR(255),
                         created_at DATETIME,
                         creator_id BIGINT,
                         launched_at DATETIME,
                         deadline DATETIME,
                         goal FLOAT,
                         currency VARCHAR(255),
                         currency_symbol VARCHAR(255),
                         current_currency VARCHAR(255),
                         location_country VARCHAR(255),
                         location_displayable_name VARCHAR(255),
                         location_name VARCHAR(255),
                         location_state VARCHAR(255),
                         is_starrable BOOLEAN,
                         urlswebproject VARCHAR(255),
                         PRIMARY KEY (ID),
                         UNIQUE KEY (project_id),
                         FOREIGN KEY (category_id) REFERENCES category_mappings(category_id),
                         FOREIGN KEY (creator_id) REFERENCES creator(creator_id)
                         );""",
                                        """CREATE TABLE IF NOT EXISTS projects_status (
                        ID INT  AUTO_INCREMENT,
                        project_ID BIGINT NOT NULL,
                        scrape DateTime,
                        backers_count INT,
                        percent_funded Float,
                        pledged float,
                        usd_pledged float,
                        spotlight BOOLEAN,
                        staff_pick BOOLEAN,
                        state VARCHAR(255),
                        PRIMARY KEY (ID),
                        FOREIGN KEY (project_ID) REFERENCES projects(project_ID)
                        );""",
                                        """CREATE TABLE IF NOT EXISTS country_population (
                        ID INT  AUTO_INCREMENT,
                        code VARCHAR(50),
                        name VARCHAR(255),
                        population INT,
                        PRIMARY KEY (ID),
                        UNIQUE KEY (code)                      
                        );"""]
        self.database_ready = self.create_database()

    def create_database(self):
        """Sets up the SQL Databases ensuring the tables are created."""
        try:
            cnx = mysql.connector.connect(user=self.database_username)
            c = cnx.cursor()
            for command in self.database_creation_query:  # execute commands in the list of commands
                c.execute(command)
                cnx.commit()
            cnx.close()
            return True
            # logging.info("Database Set-up.")
        except mysql.connector.Error:
            return False

    def sql_population(self):
        """Populate SQL Databases"""
        if self.database_content is None or len(self.database_content) < 0:
            return False
        elif not self.database_ready:
            if not self.create_database():
                return False
        try:
            cnx = mysql.connector.connect(user=self.database_username, database='kickstarter')
            # Category and creator should be loaded first as projects and projects_status depend on their keys
            self.populate_category(cnx)
            self.populate_creator(cnx)
            # Projects should be loaded second as projects_status needs project_id from it
            self.populate_projects(cnx)
            self.populate_project_status(cnx)
            self.populate_country(cnx)
            cnx.close()
            return True
        except mysql.connector.Error:
            return False

    def populate_category(self, connection):
        sql = "INSERT IGNORE INTO category_mappings " \
              "(category_id, category_name, category_parent_id, category_slug)" \
              "VALUES (%s, %s, %s, %s)"
        category_columns = self.database_content.loc[:, ['categoryid', 'categoryname',
                                                         'categoryparent_id', 'categoryslug']]
        table_values = [tuple(x) for x in category_columns.itertuples(index=False, name='Pandas')]
        c = connection.cursor()
        c.executemany(sql, table_values)
        connection.commit()

    def populate_creator(self, connection):
        sql = """INSERT IGNORE INTO creator
              (creator_id, creator_name, creator_url, backed_projects,
               created_projects, comments)
              VALUES (%s, %s, %s, %s, %s, %s)"""
        creator_columns = self.database_content.loc[:, ['creatorid', 'creatorname', 'creatorurlswebuser',
                                                        'Backed', 'Created', 'Comments']]
        table_values = [tuple(x) for x in creator_columns.itertuples(index=False, name='Pandas')]
        c = connection.cursor()
        c.executemany(sql, table_values)
        connection.commit()

    def populate_projects(self, connection):
        sql = """INSERT IGNORE INTO projects
              (project_ID, name, slug, blurb, category_id, country, created_at, 
              creator_id, launched_at, deadline, goal, currency, currency_symbol,
              current_currency, location_country, location_displayable_name,
              location_name, location_state, is_starrable, urlswebproject)
              VALUES (%s, %s, %s, %s, %s, %s, FROM_UNIXTIME(%s), %s, FROM_UNIXTIME(%s), 
              FROM_UNIXTIME(%s), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        project_columns = self.database_content.loc[:, ['id', 'name', 'slug', 'blurb', 'categoryid',
                                                        'country', 'created_at', 'creatorid',
                                                        'launched_at', 'deadline', 'goal', 'currency',
                                                        'currency_symbol', 'current_currency',
                                                        'locationcountry', 'locationdisplayable_name',
                                                        'locationname', 'locationstate',
                                                        'is_starrable', 'urlswebproject']]
        table_values = [tuple(x) for x in project_columns.itertuples(index=False, name='Pandas')]
        c = connection.cursor()
        c.executemany(sql, table_values)
        connection.commit()

    def populate_project_status(self, connection):
        sql = """INSERT IGNORE INTO projects_status
              (project_ID, scrape, backers_count, percent_funded, pledged, 
              usd_pledged, spotlight, staff_pick, state)
              VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        creator_columns = self.database_content.loc[:, ['id', 'scrape', 'backers_count',
                                                        'percent_funded', 'pledged', 'usd_pledged',
                                                        'spotlight', 'staff_pick', 'state']]
        table_values = [tuple(x) for x in creator_columns.itertuples(index=False, name='Pandas')]
        c = connection.cursor()
        c.executemany(sql, table_values)
        connection.commit()

    def populate_country(self, connection):
        sql = """INSERT IGNORE INTO country_population
                (code, name, population) 
                VALUES (%s, %s, %s)"""
        country_columns = self.country_content.loc[:, ['code', 'name', 'population']]
        table_values = [x for x in country_columns.itertuples(index=False, name='Pandas')]
        c = connection.cursor()
        c.executemany(sql, table_values)
        connection.commit()


def main():
    # Don't want to run set_up when importing to execute tests
    pass


if __name__ == "__main__":
    """Only run if not imported."""
    main()
