import unittest
import DataMining
from bs4 import BeautifulSoup
import SQLPopulation
import pandas as pd


class TestStringMethods(unittest.TestCase):
    def setUp(self):
        # Creator Table Information
        with open('creator.html', 'rb') as creator_file:
            creator_content = creator_file.read()
            creator_soup = BeautifulSoup(creator_content, 'html.parser')
        self.creator_html = creator_soup.find_all('li', class_="nav--subnav__item")
        self.creator_dict = {'user': 'url'}
        self.scraper = DataMining.KickstarterScraper()
        # Project Table Information
        with open('projects.html', 'rb') as project_file:
            project_content = project_file.read()
            project_soup = BeautifulSoup(project_content, 'html.parser')
        self.project_cards = project_soup.find_all(class_="js-react-proj-card")  # ResultSet object
        # SQL DataFrame
        self.sql_dataframe = pd.DataFrame(columns=['test'])
        self.database_user = 'ITC'
        self.sql_population_dataframe = pd.DataFrame(columns=['id'])

    def test_build_website(self):
        url_expected = ['https://www.kickstarter.com/discover/advanced?state=live&sort=most_funded&category_id=games&page=1',
                        'https://www.kickstarter.com/discover/advanced?state=live&sort=most_funded&category_id=games&page=2',
                        'https://www.kickstarter.com/discover/advanced?state=live&sort=most_funded&category_id=games&page=3']
        self.assertEqual(self.scraper.build_website(['games'], "most_funded", 3), url_expected)
        self.assertEqual(self.scraper.build_website(['art'], "most_funded", 1),
                         ['https://www.kickstarter.com/discover/advanced?state=live&sort=most_funded&category_id=art&page=1'])

    def test_extract_content_from_url(self):
        """Test the extract content method"""
        url_bad = "https://bdkjsdldfkjds_Bad_url"
        url_good = "http://www.google.com"
        self.assertIsNone(self.scraper.extract_content_from_url(url_bad))
        self.assertIsNotNone(self.scraper.extract_content_from_url(url_good))

    def test_formatter(self):
        """Test formatter methodd"""
        sample = "false true"
        result = "False True"
        self.assertEqual(self.scraper.formatter(sample), result)

    def test_attribute_dict(self):
        dictionary_of_values = {'id': 133, 'name': 'TEST', 'location': {'name': 'Place',
                                                                        'state': 'OH'}}
        to_include = ['id', 'name', ['location', ['name', 'country', 'state', 'displayable_name']]]
        result = {'id': 133, 'name': 'TEST', 'locationname': 'Place', 'locationstate': 'OH',
                  'locationdisplayable_name': None, 'locationcountry': None}
        join_value = ""
        self.assertEqual(self.scraper.attribute_dict(dictionary_of_values, to_include, key_join=join_value), result)

    def test_creator_content(self):
        expected = {'user': 'url', 'Backed': '15', 'Comments': '399', 'Created': '2'}
        self.assertEqual(self.scraper.creator_content(self.creator_html, self.creator_dict,
                                                      self.scraper.creator_labels), expected)

    def test_fill_df(self):
        """Test the function that populates the dataframe with project info"""
        df = self.scraper.fill_df(self.project_cards)  # fill the database with projects content
        self.assertEqual(df.shape, (24, 32))
        self.assertTrue(df.loc[df.id == 23734280, 'staff_pick'].iloc[0])
        self.assertEqual(df.loc[df.id == 23734280, 'backers_count'].iloc[0], 115)

    def test_sql(self):
        my_database = SQLPopulation.SQLDatabaseManagement(self.database_user, self.sql_dataframe, self.sql_population_dataframe)
        self.assertTrue(my_database.database_ready)

    def test_country(self):
        dummy_codes = ['BZ', 'GB', 'US', 'FR', 'NL'] #Input
        function_output = self.scraper.get_countries_population(dummy_codes)
        self.assertEqual(function_output.shape, (5,3))
        self.assertIsInstance(function_output, pd.DataFrame)
        self.assertIsInstance(function_output.iloc[0,0], str)
        self.assertIsInstance(function_output.iloc[0,1], str)
        self.assertIsInstance(function_output.iloc[0,2], int)




if __name__ == '__main__':
    unittest.main()
