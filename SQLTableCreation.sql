SET default_storage_engine = MyISAM;

CREATE DATABASE IF NOT EXISTS kickstarter;

USE kickstarter;

DROP TABLE IF EXISTS category_mappings
CREATE TABLE IF NOT EXISTS category_mappings (
	ID INT  AUTO_INCREMENT,
	category_id INT NOT NULL,
	category_name VARCHAR(255) NOT NULL,
	category_parent_id INT,
	category_slug VARCHAR(255) NOT NULL,
	PRIMARY KEY (ID),
    UNIQUE KEY(category_id)
	);

CREATE TABLE IF NOT EXISTS creator (
	ID INT  AUTO_INCREMENT,
	creator_id BIGINT,
	creator_name VARCHAR(255),
	creator_url VARCHAR(255),
	backed_projects INT default 0,
    created_projects INT default 0,
    comments INT default 0,
	PRIMARY KEY (ID),
    UNIQUE KEY(creator_id)
	);

CREATE TABLE IF NOT EXISTS projects (
	ID INT  AUTO_INCREMENT,
	project_ID BIGINT NOT NULL,
	name VARCHAR(255),
	slug VARCHAR(255),
	blurb TEXT,
	category_id INT,
	country VARCHAR(255),
	created_at DATETIME,
	creator_id BIGINT,
	launched_at DATETIME,
	deadline DATETIME,
	goal FLOAT,
	currency VARCHAR(255),
	currency_symbol VARCHAR(255),
	current_currency VARCHAR(255),
	location_country VARCHAR(255),
	location_displayable_name VARCHAR(255),
	location_name VARCHAR(255),
	location_state VARCHAR(255),
	is_starrable BOOLEAN,
	urlswebproject VARCHAR(255),
	PRIMARY KEY (ID),
    UNIQUE KEY (project_id),
	FOREIGN KEY (category_id) REFERENCES category_mappings(category_id),
    FOREIGN KEY (creator_id) REFERENCES creator(creator_id)

	);



CREATE TABLE IF NOT EXISTS projects_status (
	ID INT  AUTO_INCREMENT,
	project_ID BIGINT NOT NULL,
	scrape DateTime,
	backers_count INT,
	percent_funded Float,
	pledged float,
	usd_pledged float,
	spotlight BOOLEAN,
	staff_pick BOOLEAN,
	state VARCHAR(255),
	PRIMARY KEY (ID),
	FOREIGN KEY (project_ID) REFERENCES projects(project_ID)
	);

describe category_mappings;
describe projects_status;
describe projects;

