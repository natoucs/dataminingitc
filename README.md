# Kickstarter web scraper



# Use

* **Extracts information about each project on a Kickstarter projects page**
* **Stores the information into a CSV file or a MySQL database**

For each project, information retrieved includes, and is not restricted to: 

* Project title 
* Description
* Creator and 
* Amount pledged
* Percentage funded
* Number of days since creation
* Category and subcategory
* Location

Example page our python script can scrap: https://www.kickstarter.com/discover/advanced?category_id=16

An API is used to enrich the data by adding the project's country population.
The API can be found at: http://api.population.io

This is a finished project.

## Getting Started

To run the web scraper, you can just clone the repo on your desktop and run DataMining.py in your terminal.

You can specify some parameters by using flags when running the web scraper.

They are listed below as [flag] [name of flag] [default value] [description]:

* -c category 'Technology' (category of projects to scrap, if not given all will be searched.)
* -s sort 'most_funded' (how the projects are sorted in the database and CSV file)
* -p pages 1 (number of pages to scrap from)
* -u database_username 'ITC' (Username for connecting to the database)
* -w write_to_file 'False' (Whether to write the results to a CSV file or not)

Other things you need to know:

* If a parameter is not specified, its default value is used.
If you use a MySQL database, the username ITC is needed. No password is needed.
* list of choices for sort mode:
['most_funded', 'newest', 'most_backed', 'end_date', 'popularity', 'magic']
* list of choices for categories:
{'Art': 1, 'Comics': 3, 'Dance': 6, 'Design': 7,
'Fashion': 9, 'Food': 10, 'Games': 12, 'Journalism': 13,
'Music': 14, 'Photography': 15, 'Technology': 16,
'Theater': 17, 'Publishing': 18, 'Crafts': 26}
 
### Prerequisites

* Python
* MySQL

The following Python libraries are used:

* import requests
* from bs4 import BeautifulSoup
* import ast
* import pandas as pd
* import click
* import datetime
* import os
* import logging
* import json
* import SQLPopulation

### Installing

No installation is necessary to use this scraper.

## Running the tests

Go to the tests folder of the project and run DataMiningUnitTests.py in it without specifying any parameters.
The tests are done on the two saved html web pages in the test folder.

## Deployment

One way to use our scraper is:

1. Run an AWS server 

2. Download our repository on it 

3. Run DataMining.py or set up a crontab to run it regularly

## Authors

* **Jenn Hobohm** 
* **Nathan Cavaglione** 

## Contributions

If you want to contribute, email nathan dot cavaglione at gmail dot com.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Israel Tech Challenge: for pushing us to build this project.